TEST APP
========

### Cloudron
APPSTORE_ORIGIN=https://api.staging.cloudron.io cloudron build

### Test locally
docker build -t cloudron/test .
docker run --name test -p 7777:7777 -p 7778:7778 cloudron/test

Creating New version
====================
The e2e tests use the 'app' in staging.

export APPSTORE_ORIGIN=https://api.staging.cloudron.io
cloudron build
cloudron submit

App needs to be approved for the auto update test.

