'use strict';

exports = module.exports = tcpBomb;

var async = require('async'),
    net = require('net');

function tcpBomb(ip, port, times, timeout, callback) {
    async.times(times, function (n, done) {
        var client = new net.Socket();
        client.setTimeout(timeout);

        client.connect(port, ip, function() {
            client.destroy();
            done(null, 'Connected');
        });

        client.on('timeout', function () {
            client.destroy();
            done(null, 'Timeout');
        });

        client.on('error', function (error) {
            done(null, 'Error: ' + error.message);
        });
    }, function (error, result) {
        var x = result.reduce(function (ac, val) {
            if (val === 'Connected') ++ac.connected;
            else if (val === 'Timeout') ++ac.timeout;
            else ++ac.error;

            return ac;
        }, { connected: 0, error: 0, timeout: 0 });
        return callback(null, x);
    });
}

