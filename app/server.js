'use strict';

const async = require('async'),
    dns = require('dns'),
    dgram = require('dgram'),
    Database = require('better-sqlite3'),
    Docker = require('dockerode'),
    exec = require('child_process').exec,
    fs = require('fs'),
    http = require('http'),
    Imap = require('imap'),
    ldap = require('ldapjs'),
    mkdirp = require('mkdirp'),
    mkfifoSync = require('mkfifo').mkfifoSync,
    { MongoClient } = require('mongodb'),
    mysql = require('mysql'),
    net = require('net'),
    nodemailer = require('nodemailer'),
    once = require('once'),
    pg = require('pg'),
    redis = require('redis'),
    safe = require('safetydance'),
    smtpTransport = require('nodemailer-smtp-transport'),
    superagent = require('superagent'),
    tcpBomb = require('./tcpbomb.js'),
    tls = require('tls'),
    url = require('url'),
    util = require('util'),
    WebSocketServer = require('ws').WebSocketServer;

function expectError(tag, func) {
    return function (callback) {
        func(function (error) {
            if (error) console.log(tag, error.message);

            if (!error) return callback(new Error('Expecting error'));

            callback();
        });
    };
}

function populateMySql(callback) {
    console.log('Populating MySQL');
    var conn = mysql.createConnection(process.env.CLOUDRON_MYSQL_URL);
    async.series([
        conn.query.bind(conn, 'CREATE TABLE IF NOT EXISTS test(name VARCHAR(10) NOT NULL, value VARCHAR(10), PRIMARY KEY(name))'),
        conn.query.bind(conn, 'INSERT IGNORE INTO test (name, value) VALUES(?, ?)', [ 'cloudron', 'rocks' ]),
        conn.end.bind(conn)
    ], callback);
}

function populatePostgresql(callback) {
    console.log('Populating PostgreSQL');
    const pool = new pg.Pool({
        user: process.env.CLOUDRON_POSTGRESQL_USERNAME,
        password: process.env.CLOUDRON_POSTGRESQL_PASSWORD,
        host: process.env.CLOUDRON_POSTGRESQL_HOST,
        port: process.env.CLOUDRON_POSTGRESQL_PORT,
        database: process.env.CLOUDRON_POSTGRESQL_DATABASE,
        ssl: false
    });

    pool.connect(function (error, client) {
        if (error) return callback(error);

        async.series([
            client.query.bind(client, 'CREATE TABLE IF NOT EXISTS test(name VARCHAR(10) NOT NULL, value VARCHAR(10), PRIMARY KEY(name))'),
            client.query.bind(client, 'INSERT INTO test(name,value) SELECT \'cloudron\', \'rocks\' WHERE NOT EXISTS (SELECT 1 FROM test WHERE name=\'cloudron\')'),
            function (callback) { client.end(); return callback(); }
        ], callback);
    });
}

async function populateMongodb() {
    console.log('Populating MongoDB');
    const client = new MongoClient(process.env.CLOUDRON_MONGODB_URL);
    await client.connect();
    const db = client.db(process.env.CLOUDRON_MONGO_DATABASE);
    const collection = db.collection('test', { strict: false });
    const result = await collection.insertOne({ 'cloudron': 'rocks' });
    if (!result.insertedId) throw new Error(`Unexpected insertedCount ${result.insertedId}`);
    client.close();
}

function populateLocalStorage(callback) {
    console.log('Populating LocalStorage');

    if (fs.existsSync('/app/data/dir')) {
        console.log('Already populated');
        return callback();
    }

    fs.mkdirSync('/app/data/dir', { force: true });
    fs.writeFileSync('/app/data/dir/data.json', JSON.stringify({ 'cloudron' : 'rocks' }, null, 4), 'utf8');

    fs.mkdirSync('/app/data/emptydir', { force: true });  // check that dirs are preserved correctly

    fs.writeFileSync('/app/data/script.sh', 'some executable file', 'utf8');
    fs.chmodSync('/app/data/script.sh', parseInt('0755', 8));

    mkfifoSync('/app/data/fifo', parseInt('0600', 8));

    fs.symlinkSync('/app/data/dir', '/app/data/symlink');

    exec('cp -r /app/code/mangle /app/data', callback);
}

async function populateRedis() {
    console.log('Populating redis');
    const client = redis.createClient({url: process.env.CLOUDRON_REDIS_URL.replace('redisuser','')}); // the new redis module does not like fake username
    await client.connect();
    await client.set('cloudron', 'rocks');
    await client.disconnect();
}

async function populateSqlite() {
    console.log('Populating sqlite');

    const db = new Database('/app/data/sqlite_nowal.db', {});
    db.exec('CREATE TABLE test (id TEXT)');
    db.exec('INSERT INTO test VALUES (\'Cloudron\')');
    //db.close(); // not closed intentionally

    const db2 = new Database('/app/data/sqlite_wal.db', {});
    db2.pragma('journal_mode = WAL');
    db2.exec('CREATE TABLE test (id TEXT)');
    db2.exec('INSERT INTO test VALUES (\'Cloudron\')');
    //db2.close(); // not closed intentionally
}

function populateAddons(callback) {
    var result = { };

    function recordResult(tag, func) {
        return function (callback) {
            func(function (error) {
                result[tag] = error ? error.message : 'OK';
                callback();
            });
        };
    }

    async.series([
        recordResult('mysql', populateMySql),
        recordResult('postgresql', populatePostgresql),
        recordResult('mongodb', util.callbackify(populateMongodb)),
        recordResult('localstorage', populateLocalStorage),
        recordResult('redis', util.callbackify(populateRedis)),
        recordResult('sqlite', util.callbackify(populateSqlite))
    ], function () {
        callback(null, result);
    });
}

function checkMySql(callback) {
    console.log('Checking mysql');

    var conn = mysql.createConnection(process.env.CLOUDRON_MYSQL_URL);
    conn.query('SELECT * FROM test WHERE name = ?', [ 'cloudron' ], function (error, results) {
        conn.end();

        if (error) return callback(error);
        if (results.length === 0) return callback(new Error('Query returned empty result'));
        if (results[0].value !== 'rocks') return callback(new Error('Value is ' + results[0].value));
        return callback(null);
    });
}

function checkPostgresql(callback) {
    console.log('Checking postgresql');

    const pool = new pg.Pool({
        user: process.env.CLOUDRON_POSTGRESQL_USERNAME,
        password: process.env.CLOUDRON_POSTGRESQL_PASSWORD,
        host: process.env.CLOUDRON_POSTGRESQL_HOST,
        port: process.env.CLOUDRON_POSTGRESQL_PORT,
        database: process.env.CLOUDRON_POSTGRESQL_DATABASE,
        ssl: false
    });

    pool.connect(function (error, client) {
        if (error) return callback(error);

        client.query('SELECT * FROM test WHERE name = $1', [ 'cloudron' ], function (error, result) {
            if (error) return callback(error);
            if (result.rows.length === 0) return callback(new Error('Query returned empty result'));
            if (result.rows[0].value !== 'rocks') return callback(new Error('Value is ' + result.rows[0].value));

            const exts = 'hstore,pg_trgm,unaccent,citext,btree_gin,btree_gist,postgres_fdw,pg_stat_statements,plpgsql,uuid-ossp,pgcrypto,postgis,pgrouting,postgis_topology,fuzzystrmatch,postgis_tiger_geocoder,postgis_sfcgal,postgis_raster,address_standardizer,address_standardizer_data_us,ogr_fdw,cube,earthdistance,vectors';
            async.eachSeries(exts.split(','), function (ext, iteratorCallback) {
                client.query(`CREATE EXTENSION IF NOT EXISTS "${ext}"`, [], function (error) {
                    if (error) console.log(`Could not create ext ${ext}`, error);

                    iteratorCallback(error);
                });
            }, function (error) {
                client.end();
                return callback(error);
            });
        });
    });
}

function checkMongodb(callback) {
    console.log('Checking mongodb');

    MongoClient.connect(process.env.CLOUDRON_MONGODB_URL, function (error, client) {
        if (error) return callback(error);

        var db = client.db(process.env.CLOUDRON_MONGO_DATABASE);
        var collection = db.collection('test');
        collection.findOne({ 'cloudron': 'rocks' }, function (error, item) {
            client.close();

            if (error) return callback(error);
            if (!item) return callback(new Error('Query returned empty result'));

            callback(null);
        });
    });
}

function checkSqlite(callback) {
    console.log('Checking sqlite');

    try {
        const db = new Database('/app/data/sqlite_nowal.db', {});
        const row = db.prepare('SELECT * FROM test').all();
        if (row.length === 0) callback(new Error('NOWAL: no rows'));
        if (row[0].id !== 'Cloudron') callback(new Error('NOWAL: ID does not match'));
    } catch (error) {
        return callback(new Error(`Error when querying nowal db: ${error.message}`));
    }

    try {
        const db2 = new Database('/app/data/sqlite_wal.db', {});
        const row2 = db2.prepare('SELECT * FROM test').all();
        if (row2.length === 0) callback(new Error('WAL: no rows'));
        if (row2[0].id !== 'Cloudron') callback(new Error('WAL: ID does not match'));
    } catch (error) {
        return callback(new Error(`Error when querying wal db: ${error.message}`));
    }

    callback(null);
}

function checkNetAdmin(callback) {
    exec('sudo iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE', function (error) {
        if (error) return callback(new Error('checkNetAdmin: ' + error.message));

        callback();
    });
}

function checkVaapi(callback) {
    console.log('Checking VAAPI');

    if (fs.existsSync('/dev/dri')) return callback();

    callback(new Error('Could not find /dev/dri'));
}

function checkOidc(callback) {
    console.log('Checking Oidc');

    if (!process.env.CLOUDRON_OIDC_DISCOVERY_URL) return callback(new Error('CLOUDRON_OIDC_DISCOVERY_URL environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_ISSUER) return callback(new Error('CLOUDRON_OIDC_ISSUER environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_AUTH_ENDPOINT) return callback(new Error('CLOUDRON_OIDC_AUTH_ENDPOINT environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_TOKEN_ENDPOINT) return callback(new Error('CLOUDRON_OIDC_TOKEN_ENDPOINT environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_KEYS_ENDPOINT) return callback(new Error('CLOUDRON_OIDC_KEYS_ENDPOINT environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_PROFILE_ENDPOINT) return callback(new Error('CLOUDRON_OIDC_PROFILE_ENDPOINT environment variable is empty or missing'));
    // if (!process.env.CLOUDRON_OIDC_LOGOUT_URL) return callback(new Error('CLOUDRON_OIDC_LOGOUT_URL environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_CLIENT_ID) return callback(new Error('CLOUDRON_OIDC_CLIENT_ID environment variable is empty or missing'));
    if (!process.env.CLOUDRON_OIDC_CLIENT_SECRET) return callback(new Error('CLOUDRON_OIDC_CLIENT_SECRET environment variable is empty or missing'));

    superagent.get(process.env.CLOUDRON_OIDC_DISCOVERY_URL)
        .disableTLSCerts()
        .end(function (error, result) {
            if (error) return callback(new Error('Failed to get oidc discovery document'));

            if (result.body.issuer !== process.env.CLOUDRON_OIDC_ISSUER) return callback(new Error('Discovery document has wrong issuer'));
            if (result.body.authorization_endpoint !== process.env.CLOUDRON_OIDC_AUTH_ENDPOINT) return callback(new Error('Discovery document has wrong authorization_endpoint'));
            if (result.body.userinfo_endpoint !== process.env.CLOUDRON_OIDC_PROFILE_ENDPOINT) return callback(new Error('Discovery document has wrong userinfo_endpoint'));
            if (result.body.token_endpoint !== process.env.CLOUDRON_OIDC_TOKEN_ENDPOINT) return callback(new Error('Discovery document has wrong token_endpoint'));

            callback(null);
        });
}

function checkDocker(callback) {
    console.log('Checking docker');

    if (!process.env.CLOUDRON_DOCKER_HOST) return callback(new Error('CLOUDRON_DOCKER_HOST environment variable is empty or missing'));

    const dockerURL = url.parse(process.env.CLOUDRON_DOCKER_HOST);
    const docker = new Docker({host: dockerURL.hostname, port: dockerURL.port});

    docker.listContainers(function (error, containers) {
        if (error) return callback(error);
        if (containers.length === 0) return callback(new Error('Empty response'));

        callback(null);
    });
}

function checkTls(callback) {
    console.log('Checking tls addon');

    if (!safe.fs.existsSync('/etc/certs/tls_cert.pem')) return callback(new Error('cert file is missing'));
    if (!safe.fs.existsSync('/etc/certs/tls_key.pem')) return callback(new Error('key file is missing'));

    const cert = safe.fs.readFileSync('/etc/certs/tls_cert.pem', { encoding: 'utf8' });
    const key = safe.fs.readFileSync('/etc/certs/tls_key.pem', { encoding: 'utf8' });
    if (!cert || !key) return callback(new Error('failed to create cert/key'));

    // -checkhost checks for SAN or CN exclusively. SAN takes precedence and if present, ignores the CN.
    const fqdn = process.env.CLOUDRON_APP_DOMAIN;

    let result = safe.child_process.execSync(`openssl x509 -noout -checkhost "${fqdn}"`, { encoding: 'utf8', input: cert });
    if (result === null) return callback(new Error('Unable to get certificate subject:' + safe.error.message));

    if (result.indexOf('does match certificate') === -1) return callback(new Error(`Certificate is not valid for this domain. Expecting ${fqdn}`));

    // check if public key in the cert and private key matches. pkey below works for RSA and ECDSA keys
    const pubKeyFromCert = safe.child_process.execSync('openssl x509 -noout -pubkey', { encoding: 'utf8', input: cert });
    if (pubKeyFromCert === null) return callback(new Error(`Unable to get public key from cert: ${safe.error.message}`));

    const pubKeyFromKey = safe.child_process.execSync('openssl pkey -pubout', { encoding: 'utf8', input: key });
    if (pubKeyFromKey === null) return callback(new Error(`Unable to get public key from private key: ${safe.error.message}`));

    if (pubKeyFromCert !== pubKeyFromKey) return callback(new Error('Public key does not match the certificate.'));

    // check expiration
    result = safe.child_process.execSync('openssl x509 -checkend 0', { encoding: 'utf8', input: cert });
    if (!result) return callback(new Error('Certificate has expired.'));

    callback(null);
}

function checkTurn(callback) {
    console.log('Checking turn');

    if (!process.env.CLOUDRON_TURN_SERVER) return callback(new Error('CLOUDRON_TURN_SERVER environment variable is empty or missing'));
    if (!process.env.CLOUDRON_TURN_PORT) return callback(new Error('CLOUDRON_TURN_PORT environment variable is empty or missing'));
    if (!process.env.CLOUDRON_TURN_TLS_PORT) return callback(new Error('CLOUDRON_TURN_TLS_PORT environment variable is empty or missing'));
    if (!process.env.CLOUDRON_TURN_SECRET) return callback(new Error('CLOUDRON_TURN_SECRET environment variable is empty or missing'));

    var socket = new net.Socket();
    socket.connect(process.env.CLOUDRON_TURN_PORT, process.env.CLOUDRON_TURN_SERVER);
    socket.on('connect',  function () {
        console.log('checkTurn: socket connected');
        socket.destroy(); // end() emits error
        callback(null);
    }).on('error', function (error) {
        console.log('checkTurn: socket errored', error);
        callback(error);
    });
}

function doLdapSearch(callback) {
    var client = ldap.createClient({ url: process.env.CLOUDRON_LDAP_URL });
    client.bind(process.env.CLOUDRON_LDAP_BIND_DN, process.env.CLOUDRON_LDAP_BIND_PASSWORD, function (error) {
        if (error) return callback(new Error('ldap search failed at bind: ' + error.message));

        client.search(process.env.CLOUDRON_LDAP_USERS_BASE_DN, { scope: 'sub' }, function (error, res) {
            if (error) return callback(new Error('could not search after bind: ' + error.message));

            var entries = [ ];
            res.on('searchEntry', function(entry) { entries.push(entry); });
            res.on('error', callback);
            res.on('end', function () {
                callback();
            });
        });
    });
}

function doLdapAuth(username, password, callback) {
    var client = ldap.createClient({ url: process.env.CLOUDRON_LDAP_URL });
    client.bind('cn=' + username + ',' + process.env.CLOUDRON_LDAP_USERS_BASE_DN, password, function (error) {
        if (error) return callback(new Error('ldap auth failed: ' + error.message));

        callback();
    });
}

function checkLdap(username, password, callback) {
    console.log('Checking ldap');

    async.series([
        doLdapSearch,
        doLdapAuth.bind(null, username, password),
        expectError('checkLdap1', doLdapAuth.bind(null, username + 'x', password)),
        expectError('checkLdap2', doLdapAuth.bind(null, username, password + 'x'))
    ], callback);
}

var gCheckedSmtp = false; // cache success result to avoid spam. note we have to keep retrying since the auth in smtp addon takes time to reload/refresh
var gRespondToHealthcheck = true;
var gLogBombInterval = null;

function checkSmtp(from, callback) {
    console.log('Checking smtp');

    if (gCheckedSmtp) return callback();

    var transport = nodemailer.createTransport(smtpTransport({
        host: process.env.CLOUDRON_MAIL_SMTP_SERVER,
        port: process.env.CLOUDRON_MAIL_SMTP_PORT, // this value comes from mail container
        auth: {
            user: process.env.CLOUDRON_MAIL_SMTP_USERNAME,
            pass: process.env.CLOUDRON_MAIL_SMTP_PASSWORD
        }
    }));

    var mailOptions = {
        from: from,
        to: 'test@cloudron.io',
        cc: process.env.CLOUDRON_MAIL_TO, // from the imap addon
        subject: 'E2E Test mail',
        text: 'This mail is from the e2e test. If you receive this sendmail addon works'
    };

    transport.sendMail(mailOptions, function (error) {
        if (error) return callback(error);

        gCheckedSmtp = true;
        callback();
    });
}

function checkSieve(username, password, callback) {
    console.log('checkSieve: Auth with ' + username + ' and password ' + password);
    var authString = 'AUTHENTICATE "PLAIN" "' + new Buffer('\0' + username + '\0' + password).toString('base64') + '"';
    let tlsSocket = null;

    callback = once(callback);

    function startTls(socket) {
        console.log('STARTTLS');

        tlsSocket = tls.connect({ socket: socket, rejectUnauthorized: false });

        tlsSocket.on('secureConnect', function () {
            console.log('checkSieve: Now securely connected.');
        });
        tlsSocket.on('data', function (data) {
            console.log('checkSieve: TLS DATA: ', data.toString('utf8'));
            if (data.toString('utf8').includes('TLS negotiation successful')) {
                console.log('checkSieve: Sending authentication ' + authString);
                tlsSocket.write(authString + '\n');
            } else if (data.toString('utf8').startsWith('OK "Logged in."')) {
                console.log('checkSieve: Successfully authenticated');
                tlsSocket.end();
                callback();
            } else if (data.toString('utf8').startsWith('NO "Authentication failed."')) {
                console.log('checkSieve: Authentication failed');
                tlsSocket.end();
                callback(new Error('Authentication failed'));
            }
        });

        tlsSocket.on('end', function () {
            console.log('checkSieve: socket disconnected');
            callback(new Error('Authentication failed'));
        });
    }

    var socket = new net.Socket();
    socket.connect(process.env.CLOUDRON_EMAIL_SIEVE_PORT, process.env.CLOUDRON_EMAIL_SIEVE_SERVER);
    socket.on('connect',  function () {
        console.log('checkSieve: socket connected');
        setTimeout(socket.write.bind(socket, 'STARTTLS\n'), 1000);
    }).on('data', function (data) {
        if (data.toString('utf8').startsWith('OK "Begin TLS negotiation now."')) {
            console.log('Starting TLS');
            startTls(socket);
        } else if (!tlsSocket) {
            // console.log('PLAIN DATA:', data.toString('utf8'));
        }
    }).on('error', function (error) {
        console.log('checkSieve: socket errored', error);
        callback(error);
    });
}

function doMailboxLdapAuth(email, password, callback) {
    const domain = email.split('@')[1];

    var client = ldap.createClient({ url: process.env.CLOUDRON_LDAP_URL });
    client.bind(`cn=${email},domain=${domain},${process.env.CLOUDRON_EMAIL_LDAP_MAILBOXES_BASE_DN}`, password, function (error) {
        if (error) return callback(new Error('Could not mailbox ldap auth:' + error.message));

        client.unbind();

        callback();
    });
}

function checkEmail(username, password, callback) {
    console.log('Checking email');

    if (!process.env.CLOUDRON_EMAIL_SMTP_SERVER) return callback(new Error('EMAIL_SMTP_SERVER missing'));
    if (!process.env.CLOUDRON_EMAIL_SMTP_PORT) return callback(new Error('EMAIL_SMTP_PORT is missing'));
    if (!process.env.CLOUDRON_EMAIL_IMAP_SERVER) return callback(new Error('EMAIL_IMAP_SERVER missing'));
    if (!process.env.CLOUDRON_EMAIL_IMAP_PORT) return callback(new Error('EMAIL_IMAPS_PORT is missing'));
    if (!process.env.CLOUDRON_EMAIL_SIEVE_SERVER) return callback(new Error('EMAIL_SIEVE_SERVER missing'));
    if (!process.env.CLOUDRON_EMAIL_SIEVE_PORT) return callback(new Error('EMAIL_SIEVE_PORT is missing'));
    if (!process.env.CLOUDRON_EMAIL_DOMAIN) return callback(new Error('EMAIL_DOMAIN is missing'));
    if (!process.env.CLOUDRON_EMAIL_LDAP_MAILBOXES_BASE_DN) return callback(new Error('EMAIL_LDAP_MAILBOXES_BASE_DN is missing'));

    async.series([
        checkSieve.bind(null, username + '@' + process.env.CLOUDRON_EMAIL_DOMAIN, password),
        doMailboxLdapAuth.bind(null, username + '@' + process.env.CLOUDRON_EMAIL_DOMAIN, password),
    ], callback);
}

function checkImap(imap, callback) {
    function end(error) {
        imap.end();
        callback(error);
    }

    imap.once('ready', function() {
        imap.openBox('INBOX', false /* openReadOnly */, function (error, box) {
            if (error) return end(error);
            // sometimes it is more than 1 because check can be run multiple times
            if (!box.messages.total) return end(new Error('Expecting atleast one mail in inbox. Saw ' + box.messages.total));

            var f = imap.seq.fetch('1:1', { bodies: [ 'TEXT' ] });
            f.on('message', function (msg /*, seq */) {
                msg.on('body', function (stream /*, info */) {
                    var buffer = '';
                    stream.on('data', function (chunk) { buffer += chunk.toString('utf8'); });
                    stream.on('end', function () {
                        if (buffer.indexOf('This mail is from the e2e test') !== -1) return end();

                        end(new Error('Failed to get message body: ' + buffer));
                    });

                    stream.on('error', callback);
                });
            });

            f.on('end', function () { });
            f.on('error', callback);
        });
    });

    imap.once('error', callback);
    imap.once('end', function () { });

    imap.connect();
}

function checkRecvmail(callback) {
    console.log('Checking recvmail');

    if (!process.env.CLOUDRON_MAIL_TO) return callback(new Error('MAIL_TO missing'));
    if (!process.env.CLOUDRON_MAIL_TO_DOMAIN) return callback(new Error('MAIL_TO_DOMAIN is missing'));

    callback = once(callback);

    const imaps = new Imap({
        user: process.env.CLOUDRON_MAIL_IMAP_USERNAME,
        password: process.env.CLOUDRON_MAIL_IMAP_PASSWORD,
        host: process.env.CLOUDRON_MAIL_IMAP_SERVER,
        port: process.env.CLOUDRON_MAIL_IMAPS_PORT,
        tls: true,
        tlsOptions: {
            rejectUnauthorized: false
        }
    });

    const imap = new Imap({
        user: process.env.CLOUDRON_MAIL_IMAP_USERNAME,
        password: process.env.CLOUDRON_MAIL_IMAP_PASSWORD,
        host: process.env.CLOUDRON_MAIL_IMAP_SERVER,
        port: 9393, // process.env.CLOUDRON_MAIL_IMAP_PORT, // this conflicts with email addon env var
        tls: false
    });

    async.series([
        checkImap.bind(null, imaps),
        checkImap.bind(null, imap)
    ], callback);
}

async function checkRedis() {
    console.log('checking redis');

    const client = redis.createClient({url: process.env.CLOUDRON_REDIS_URL.replace('redisuser','')}); // the new redis module does not like fake username
    await client.connect();
    const result = await client.get('cloudron');
    if (!result) throw new Error('redis result is null');
    if (result.toString() !== 'rocks') throw new Error('unexpected reply : ' + JSON.stringy(result));

    await client.disconnect();
}

function checkExposedTcpPorts(host, ports, callback) {
    console.log('Checking exposed tcp ports', host, ports);

    function test(port, callback) {
        const client = new net.Socket();

        callback = once(callback);

        const done = (error) => {
            client.destroy();
            callback(error);
        };

        const data = [ ];
        client.connect(port, host, function() {
            console.log('connected to ' + port);
        });
        client.on('data', function (chunk) { data.push(chunk); });
        client.on('close', function () {
            const response = Buffer.concat(data).toString('utf8');
            if (response === 'from port ' + port) return done();
            console.log('unexpected response:', response);
            done(new Error('bad response'));
        });
        client.on('error', done);
    }

    async.eachSeries(ports, test, callback);
}

function checkExposedUdpPorts(host, ports, callback) {
    console.log('Checking exposed udp ports', host, ports);

    function test(port, callback) {
        const udpClient = dgram.createSocket('udp4');

        const done = (error) => {
            udpClient.close();
            callback(error);
        };

        udpClient.on('message', function (message, remote) {
            console.log('incoming udp message from ' + remote.address + ':' + remote.port +' - ' + message);
            const response = message.toString('utf8');
            if (response === 'from port ' + port) return done();
            done(new Error('bad response'));
        });
        const info = Buffer.from('ping');
        udpClient.send(info, 0, info.length, port, host, function (error) {
            console.log('Sent message with error:' + error);
            if (error) return done(error);
        });
    }

    async.eachSeries(ports, test, callback);
}

// can we reach ourselves via http and port ?
function checkNetwork(callback) {
    const expectedEnvVars = [
        'ECHO_SERVER_PORT_TCP_RANGE_COUNT',
        'ECHO_SERVER_PORT_TCP_RANGE',
        'ECHO_SERVER_PORT_UDP_RANGE_COUNT',
        'ECHO_SERVER_PORT_UDP_RANGE',
        'ECHO_SERVER_PORT_TCP',
        'ECHO_SERVER_PORT_UDP',
        'DNS_SERVER_PORT_UDP'
    ];

    for (const envvar of expectedEnvVars) {
        if (!process.env[envvar]) return callback(new Error(`${envvar} is not defined`));
    }

    superagent.get(process.env.CLOUDRON_APP_ORIGIN + '/healthcheck')
        .disableTLSCerts()
        .end(function (error, result) {
            if (error || !result) return callback(error);

            if (result.body.status !== 'OK') return callback(new Error('Expecting OK in body'));

            console.log('http connectivity worked');

            const tcpPorts = Array.from({ length: parseInt(process.env.ECHO_SERVER_PORT_TCP_RANGE_COUNT, 10) }, (_, a) => a + parseInt(process.env.ECHO_SERVER_PORT_TCP_RANGE, 10));
            const udpPorts = Array.from({ length: parseInt(process.env.ECHO_SERVER_PORT_UDP_RANGE_COUNT, 10) }, (_, a) => a + parseInt(process.env.ECHO_SERVER_PORT_UDP_RANGE, 10));

            async.series([
                checkExposedTcpPorts.bind(null, process.env.CLOUDRON_APP_DOMAIN, [ parseInt(process.env.ECHO_SERVER_PORT_TCP, 10) ]),
                checkExposedTcpPorts.bind(null, process.env.CLOUDRON_APP_DOMAIN, tcpPorts),
                checkExposedUdpPorts.bind(null, process.env.CLOUDRON_APP_DOMAIN, [ parseInt(process.env.ECHO_SERVER_PORT_UDP, 10) ]),
                checkExposedUdpPorts.bind(null, process.env.CLOUDRON_APP_DOMAIN, udpPorts)
            ], callback);
        });
}

function checkStdEnv(callback) {
    console.log('Checking stdenv');

    if (!process.env.CLOUDRON) return callback(new Error('CLOUDRON missing'));
    if (!process.env.CLOUDRON_APP_ORIGIN) return callback(new Error('APP_ORIGIN is missing'));
    if (!process.env.CLOUDRON_APP_DOMAIN) return callback(new Error('APP_DOMAIN is missing'));
    if (!process.env.BACKEND_DOMAIN) return callback(new Error('BACKEND_DOMAIN is missing'));

    callback();
}

function checkScheduler(callback) {
    console.log('Checking scheduler');

    var data = safe.fs.readFileSync('/run/every_minute.env', 'utf8');
    if (!data) return callback(new Error('waiting for /run/every_minute.sh to appear'));

    var data2 = safe.fs.readFileSync('/app/data/every_minute.env', 'utf8');
    if (!data2) return callback(new Error('waiting for /app/data/every_minute.sh to appear'));

    if (data.indexOf('REDIS_HOST=') !== -1 &&
        data.indexOf('MYSQL_URL') !== -1 &&
        data.indexOf('POSTGRESQL_URL') !== -1 &&
        data.indexOf('MONGODB_URL') !== -1 &&
        data.indexOf('LDAP_URL') !== -1 &&
        data.indexOf('MAIL_SMTP_SERVER') !== -1 &&
        data === data2)
        return callback();

    callback(new Error('Environment variables not setup correctly : ' + JSON.stringify(data)));
}

function checkAddons(username, password, callback) {
    var result = { };

    function recordResult(tag, func) {
        return function (callback) {
            func(function (error) {
                result[tag] = error ? error.message : 'OK';
                callback();
            });
        };
    }

    async.series([
        recordResult('network', checkNetwork),
        recordResult('stdenv', checkStdEnv),
        recordResult('mysql', checkMySql),
        recordResult('postgresql', checkPostgresql),
        recordResult('mongodb', checkMongodb),
        recordResult('localstorage', checkLocalStorage),
        recordResult('ldap', checkLdap.bind(null, username, password)),
        recordResult('sendmail', checkSmtp.bind(null, process.env.CLOUDRON_MAIL_FROM)),
        recordResult('redis', util.callbackify(checkRedis)),
        recordResult('email', checkEmail.bind(null, username, password)),
        recordResult('recvmail', checkRecvmail), // this has to be after the checkSmtp which sends email to this inbox
        recordResult('scheduler', checkScheduler),
        recordResult('net_admin', checkNetAdmin),
        recordResult('vaapi', checkVaapi),
        recordResult('oidc', checkOidc),
        recordResult('docker', checkDocker),
        recordResult('turn', checkTurn),
        recordResult('tls', checkTls),
        recordResult('sqlite', checkSqlite)
    ], function () {
        callback(null, result);
    });
}

function checkLocalStorage(callback) {
    try {
        var contents = fs.readFileSync('/app/data/dir/data.json', 'utf8');
        var obj = JSON.parse(contents);
    } catch (error) {
        return callback(error);
    }

    if (obj['cloudron'] !== 'rocks') return callback(new Error('Data not preserved'));

    // symlinks are not preserved anymore
    // if (!oldVersion && fs.readlinkSync('/app/data/symlink', { encoding: 'utf8' }) !== '/app/data/dir') return callback(new Error('symlink not preserved'));

    if (!fs.existsSync('/app/data/emptydir')) return callback(new Error('emptydir not preserved'));

    var mode = parseInt('0755', 8);
    if (!fs.existsSync('/app/data/script.sh') || ((fs.statSync('/app/data/script.sh').mode & mode) !== mode)) return callback(new Error('exec bit not preserved: ' + fs.statSync('/app/data/script.sh').mode));

    var origFiles = fs.readdirSync('/app/code/mangle');
    var dataFiles = fs.readdirSync('/app/data/mangle');

    if (JSON.stringify(origFiles) !== JSON.stringify(dataFiles)) return callback(new Error('some mangled files not preserved'));

    callback(null);
}

function checkDnsbl(callback) {
    dns.resolve4('2.0.0.127.zen.spamhaus.org', function (error) {
        if (error) return callback(null, { status: error.message });
        callback(null, { status: 'OK' });
    });
}

// call this function after uploading via SFTP
function checkSftp(callback) {
    if (!fs.existsSync('/app/data/sftp_incoming.txt')) return callback(new Error('Incoming SFTP file not found'));

    if (!safe.fs.readFileSync('/app/data/sftp_incoming.txt', 'utf8')) return callback(new Error('Could not read SFTP file: ' + safe.error.message));

    if (!safe.fs.appendFileSync('/app/data/sftp_incoming.txt', ' more')) return callback(new Error('Could not write SFTP file: ' + safe.error.message));

    callback(null, { status: 'OK' });
}

function checkRatelimit(callback) {
    var result = { };

    function recordResult(tag, func) {
        return function (callback) {
            func(function (error) {
                result[tag] = error ? error.message : 'OK';
                callback();
            });
        };
    }

    function check(host, port, times, callback) {
        tcpBomb(host, port, times, 900 /* timeout */, function (error, result) {
            if (error) return callback(new Error(host + ':' + port + ':' + error.message));

            if (result.timeout) return callback(null, { status: 'OK' }); // something timedout, this is good enough

            return callback(new Error(host + ':' + port + ':' + JSON.stringify(result, null, 4)));
        });
    }

    async.series([
        recordResult('ldap', check.bind(null, process.env.CLOUDRON_LDAP_SERVER, process.env.CLOUDRON_LDAP_PORT, 550)),
        recordResult('mysql', check.bind(null, process.env.CLOUDRON_MYSQL_HOST, process.env.CLOUDRON_MYSQL_PORT, 5050)),
        recordResult('sendmail', check.bind(null, process.env.CLOUDRON_MAIL_SMTP_SERVER, process.env.CLOUDRON_MAIL_SMTP_PORT, 550)),
        recordResult('recvmail', check.bind(null, process.env.CLOUDRON_MAIL_IMAP_SERVER, process.env.CLOUDRON_MAIL_IMAP_PORT, 550))
    ], function () {
        callback(null, result);
    });
}

function populateVolume(callback) {
    fs.writeFile('/media/gnr/patience.mp3', 'song', 'utf8', callback);
}

function getVolumeStatus(callback) {
    const status = {
        mountExists: false,
        readOnly: true,
        data: false
    };

    if (fs.existsSync('/media/gnr')) status.mountExists = true;
    if (safe.fs.writeFileSync('/media/gnr/november_rain.np3', 'song', 'utf8')) status.readOnly = false;
    if (safe.fs.readFileSync('/media/gnr/patience.mp3', 'utf8') === 'song') status.data = true;

    callback(null, status);
}

function startServer() {
    const httpServer = http.createServer(function (req, res) {
        var parsedUrl = url.parse(req.url, true /* parseQueryString */);

        console.log('Handling ', req.url);

        res.send = function (statusCode, obj /*, contentType */) {
            console.log('%s statusCode: %s, response %j', req.url, statusCode, obj);

            res.writeHead(statusCode, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(obj));
        };

        res.sendFile = function (filepath) {
            var content = fs.readFileSync(filepath);
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end(content);
        };

        if (parsedUrl.pathname === '/') {
            res.sendFile(`${__dirname}/index.html`);
        } else if (parsedUrl.pathname === '/healthcheck') { // note that checkStdEnv uses this response
            if (gRespondToHealthcheck) res.send(200, { status: 'OK' });
            else res.send(500, { status: 'healthcheck stopped' });
        } else if (parsedUrl.pathname === '/healthcheck_stop') {
            gRespondToHealthcheck = false;
            res.send(200, { status: 'DONE' });
        } else if (parsedUrl.pathname === '/healthcheck_start') {
            gRespondToHealthcheck = true;
            res.send(200, { status: 'DONE' });
        } else if (parsedUrl.pathname === '/logbomb_start') {
            gLogBombInterval = setInterval(function () { console.log('A lot of logs', Date(), ('x'.repeat(1000)+'\n').repeat(20)); }, 1000);
            res.send(200, { status: 'DONE' });
        } else if (parsedUrl.pathname === '/logbomb_stop') {
            clearInterval(gLogBombInterval);
            gLogBombInterval = null;
            res.send(200, { status: 'DONE' });
        } else if (parsedUrl.pathname === '/outofmemory') {
            const buffers = [];
            setInterval(function () {
                buffers.push(Buffer.alloc(100 * 1024 * 1024, 'a')); // 100M a time
            }, 1000);
        } else if (parsedUrl.pathname === '/crash') {
            process.exit();
        } else if (parsedUrl.pathname === '/populate_addons') {
            // var result = { mysql: { }, postgresql: { }, mongodb: { } };
            populateAddons(function (error, status) {
                res.send(200, status);
            });
        } else if (parsedUrl.pathname === '/check_addons') {
            if (!parsedUrl.query || !parsedUrl.query.username || !parsedUrl.query.password) {
                return res.send(401, { status: 'Needs auth args' });
            }

            checkAddons(parsedUrl.query.username, parsedUrl.query.password, function (error, status) {
                res.send(200, status);
            });
        } else if (parsedUrl.pathname === '/check_mail_from') {
            if (!parsedUrl.query || !parsedUrl.query.mail_from) {
                return res.send(401, { status: 'Needs auth args' });
            }

            checkSmtp(parsedUrl.query.mail_from, function (error) {
                return res.send(200, { status: error ? error.message : 'OK' });
            });
        } else if (parsedUrl.pathname === '/check_dnsbl') {
            checkDnsbl(function (error, result) {
                res.send(200, result);
            });
        } else if (parsedUrl.pathname === '/check_sftp') {
            checkSftp(function (error, result) {
                res.send(200, result);
            });
        } else if (parsedUrl.pathname === '/ratelimit') {
            checkRatelimit(function(error, result) {
                res.send(200, result);
            });
        } else if (parsedUrl.pathname === '/robots.txt') {
            res.writeHead(200, {'Content-Type': 'text/plain', 'Content-Security-Policy': 'default-src \'self\';'});
            res.end('User-agent: Tarantula\nAllow: /\n', 'utf8');
        } else if (parsedUrl.pathname === '/upload') { // https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/
            // curl -v -H 'Content-Type: application/octet-stream' --data-binary @tcpbomb.js https://test.intranet.forwardbias.in/upload
            if (fs.existsSync('/tmp/upload')) fs.unlinkSync('/tmp/upload');

            var uploadFile = fs.createWriteStream('/tmp/upload');
            req
                .pipe(uploadFile)
                .on('error', function (e) {
                    console.error(e);
                    res.statusCode = 400;
                    res.end();
                });
            uploadFile.on('finish', function () {
                console.log('ok, request is done now');
                res.send(200, { size: fs.statSync('/tmp/upload').size });
            });
        } else if (parsedUrl.pathname === '/.well-known/cloudron') {
            res.writeHead(301, {'Location' : '/wellknown-works'});
            res.end();
        } else if (parsedUrl.pathname === '/proxyauth/success') {
            res.send(200, 'success'); // sent as json
        } else if (parsedUrl.pathname === '/proxyauth/headers') {
            res.send(200, { headers: req.headers });
        } else if (parsedUrl.pathname === '/volume_status') {
            getVolumeStatus(function (error, result) {
                res.send(200, result);
            });
        } else if (parsedUrl.pathname === '/populate_volume') {
            populateVolume(function (error) {
                if (error) return res.send(410, error.message);
                res.send(200);
            });
        } else if (parsedUrl.pathname === '/check_runtimedirs') { // these files are not persisted across updates
            if (!safe.fs.writeFileSync('/app/code/runtimedir/somefile', 'somedata')) return res.send(200, { status: 'error writing file in runtimedir', message: safe.error.message });

            fs.rmSync('/app/code/runtimedir/somedir', { recursive: true, force: true });
            if (!safe.fs.mkdirSync('/app/code/runtimedir/somedir')) return res.send(200, { status: 'error creating dir in in runtimedir', message: safe.error.message });

            fs.rmSync('/home/cloudron/.cache/somedir', { recursive: true, force: true });
            if (!safe.fs.mkdirSync('/home/cloudron/.cache/somedir')) return res.send(200, { status: 'error creating dir in /home/cloudron/.cache', message: safe.error.message });

            res.send(200, { status: 'OK' });
        } else if (parsedUrl.pathname === '/headers') {
            res.send(200, { headers: req.headers });
        } else if (parsedUrl.pathname === '/check_device') {
            res.send(200, { status: fs.existsSync('/dev/ttyS0') ? 'OK' : 'Cannot find ttyS0' });
        } else {
            res.send(404, { status: 'nothing here' });
        }
    });

    // add websocket echo server
    const wss = new WebSocketServer({ server: httpServer });

    wss.on('connection', function (ws) {
        ws.on('error', console.error);

        ws.on('message', function (data) {
            console.log('websocket received: %s', data);
            ws.send(data.toString());
        });
    });

    httpServer.listen(7777);

    // tcp port
    var net = require('net');
    var server = net.createServer(function connectionHandler(c) {
        c.end('from port ' + process.env.ECHO_SERVER_PORT_TCP, 'utf8'); // this output is relied by e2e also
    });
    console.log(`Listening on internal 7778 (TCP). External is ${process.env.ECHO_SERVER_PORT_TCP}`);
    server.listen(7778); // this uses 'containerPort' in manifest

    // tcp port range (no containerPort in manifest)
    const tcpPortsStart = parseInt(process.env.ECHO_SERVER_PORT_TCP_RANGE, 10);
    const tcpPortsCount = parseInt(process.env.ECHO_SERVER_PORT_TCP_RANGE_COUNT, 10);

    for (let i = 0; i < tcpPortsCount; i++) {
        const port = tcpPortsStart+i;
        const server = net.createServer(function connectionHandler(c) {
            c.end('from port ' + port, 'utf8'); // this output is relies by e2e also
        });
        console.log(`Listening on ${port} (TCP)`);
        server.listen(port);
    }

    // udp port (no containerPort in manifest)
    const udpServer = dgram.createSocket('udp4');
    udpServer.on('message', function (message, remote) {
        console.log(`incoming udp message from ${remote.address}: ${remote.port} - ${message}`);
        const info = Buffer.from(`from port ${process.env.ECHO_SERVER_PORT_UDP}`, 'utf8'); // this output is relied by e2e also
        udpServer.send(info, 0, info.length, remote.port, remote.address, function (error /*, bytes */) {
            console.log('Sent message with error:' + error);
        });
    });
    console.log(`Listening on 5000 (UDP). External is ${process.env.ECHO_SERVER_PORT_UDP}`);
    udpServer.bind(5000); // this uses 'containerPort' in manifest

    const udpPortsStart = parseInt(process.env.ECHO_SERVER_PORT_UDP_RANGE, 10);
    const udpPortsCount = parseInt(process.env.ECHO_SERVER_PORT_UDP_RANGE_COUNT, 10);

    for (let i = 0; i < udpPortsCount; i++) {
        // udp port
        const udpServer = dgram.createSocket('udp4');
        const port = udpPortsStart+i;

        udpServer.on('message', function (message, remote) {
            console.log(`incoming udp message from ${remote.address}: ${remote.port} - ${message}`);
            const info = Buffer.from(`from port ${port}`, 'utf8'); // this output is relied by e2e also
            udpServer.send(info, 0, info.length, remote.port, remote.address, function (error /*, bytes */) {
                console.log('Sent message with error:' + error);
            });
        });
        console.log(`Listening on ${port} (UDP)`);
        udpServer.bind(port);
    }

    // backend
    http.createServer(function (req, res) {
        console.log('Handling backend ', req.url);
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({ message: 'backend' }));
    }).listen(8080);

    console.log('Up and running');
}

// This function touches a folder in /app/data, containing a larger file to test that we can backup even during file changes
// the current backup fails here since tar is "interrupted" by the folder mtime change
function periodicFileChanger() {
    var TOUCHED_FOLDER = '/app/data/folder';

    mkdirp.sync(TOUCHED_FOLDER);

    // create a larger file in the folder to keep the backup busy while changing the folder mtime
    exec('fallocate -l20mb ' + TOUCHED_FOLDER + '/test.txt', function (error) {
        if (error) {
            console.error('Failed to write file:', error);
            process.exit(1);
        }

        fs.utimes(TOUCHED_FOLDER, Date.now() / 1000, Date.now() / 1000, function (error) {
            if (error) {
                console.error('Failed to touch folder:', error);
                process.exit(1);
            }

            setTimeout(periodicFileChanger, 1000);
        });
    });
}

function main() {
    fs.writeFileSync('/app/data/sftp_outbound.txt', process.env.CLOUDRON_APP_DOMAIN, 'utf8');

    startServer();
    periodicFileChanger();

    process.on('SIGTERM', process.exit.bind(process));
    process.on('SIGINT', process.exit.bind(process));
}

if (require.main === module) {
    main();
}
