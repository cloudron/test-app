#!/bin/bash

set -ue

echo "Changing ownership"
chown -R cloudron:cloudron /app/data

chown -R cloudron:cloudron /app/code/runtimedir

echo "Starting test app"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/server.js

