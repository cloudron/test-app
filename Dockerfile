FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y iptables gnutls-bin stress && \
    rm -rf /var/cache/apt /var/lib/apt/lists

COPY app /app/code
WORKDIR /app/code

RUN npm install --omit=dev && npm cache clean --force

EXPOSE 7777
EXPOSE 7778

RUN echo "cloudron ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-test-app-iptables

CMD [ "/app/code/start.sh" ]

