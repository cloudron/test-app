
'use strict';

/* global describe */
/* global before */
/* global after */
/* global it */

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent');

const LOCATION = 'test';
const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

var app;
var apiOrigin;
var accessToken;

const USERNAME = process.env.USERNAME;
const PASSWORD = process.env.PASSWORD;

if (!USERNAME || !PASSWORD) {
    console.error('USERNAME and PASSWORD env variable required');
    process.exit(1);
}

function getAppInfo(done) {
    var inspect = JSON.parse(execSync('cloudron inspect'));
    app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
    expect(app).to.be.an('object');

    // TODO this is not always correct
    apiOrigin = 'https://my.' + app.domain;

    superagent.post(`${apiOrigin}/api/v1/cloudron/login`).send({ username: USERNAME, password: PASSWORD }).end(function (error, result) {
        expect(error).to.eql(null);
        expect(result.statusCode).to.eql(200);

        accessToken = result.body.accessToken;

        done();
    });
}

function canReachApp(done) {
    superagent.get(`https://${app.fqdn}/`).end(function (error, result) {
        expect(error).to.eql(null);
        expect(result.statusCode).to.eql(200);
        done();
    });
}

function checkAddons(done) {
    superagent.get(`https://${app.fqdn}/check_addons?username=${USERNAME}&password=${encodeURIComponent(PASSWORD)}`).end(function (error, result) {
        expect(error).to.eql(null);
        expect(result.statusCode).to.eql(200);

        console.dir(result.body);

        expect(result.body.mysql).to.eql('OK');
        expect(result.body.postgresql).to.eql('OK');
        expect(result.body.redis).to.eql('OK');
        expect(result.body.mongodb).to.eql('OK');
        expect(result.body.localstorage).to.eql('OK');
        expect(result.body.stdenv).to.eql('OK');
        expect(result.body.docker).to.eql('OK');
        expect(result.body.net_admin).to.eql('OK');
        expect(result.body.scheduler).to.eql('OK');
        expect(result.body.email).to.eql('OK');
        expect(result.body.ldap).to.eql('OK');

        done();
    });
}

function waitForScheduler(done) {
    console.log('Waiting for 2mins for scheduler to be run');
    setTimeout(done, 2 * 60 * 1000);
}

describe('Test app', function () {
    this.timeout(0);

    console.log('--------------------------------------------------------------------------');
    console.log('NOTE: Email must be enabled and a mailbox with the above username must exist for the test to work');
    console.log('--------------------------------------------------------------------------');
    console.log();
    console.log();

    it('can install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can reach app', canReachApp);

    it('can set app mailbox', function (done) {
        let data = {
            mailboxName: USERNAME,
            mailboxDomain: app.domain
        };

        superagent.post(`${apiOrigin}/api/v1/apps/${app.id}/configure/mailbox`).query({ access_token: accessToken }).send(data).end(function (error, result) {
            expect(error).to.eql(null);
            expect(result.statusCode).to.eql(202);

            function waitForApp(callback) {
                superagent.get(`${apiOrigin}/api/v1/apps/${app.id}`).query({ access_token: accessToken }).end(function (error, result) {
                    if (error && !error.response) return callback(new Error('network error:' + error));
                    if (result.statusCode !== 200) return waitForApp(callback);

                    if (result.body.installationState === 'installed' && result.body.runState === 'running' && result.body.health === 'healthy') {
                        return callback(null);
                    }

                    setTimeout(waitForApp.bind(null, callback), 2000);
                });
            }

            waitForApp(done);
        });
    });

    it('can populate addons', function (done) {
        superagent.get(`https://${app.fqdn}/populate_addons`).end(function (error, result) {
            expect(error).to.eql(null);
            expect(result.statusCode).to.eql(200);

            expect(result.body.mysql).to.eql('OK');
            expect(result.body.postgresql).to.eql('OK');
            expect(result.body.redis).to.eql('OK');
            expect(result.body.mongodb).to.eql('OK');
            expect(result.body.localstorage).to.eql('OK');

            done();
        });
    });

    it('wait for 2 minutes for scheduler', waitForScheduler);
    it('can check addons', checkAddons);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('backup app twice to test potential hardlink creation', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

    it('restore app', function () { execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS); });

    it('can reach app', canReachApp);
    it('wait for 2 minutes for scheduler', waitForScheduler);
    it('can check addons', checkAddons);

    it('can uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
